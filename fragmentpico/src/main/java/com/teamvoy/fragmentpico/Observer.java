package com.teamvoy.fragmentpico;

interface Observer {

	void onValueChanged(Object o);

}
