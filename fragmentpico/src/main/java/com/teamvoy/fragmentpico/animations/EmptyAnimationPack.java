package com.teamvoy.fragmentpico.animations;

import android.view.animation.Animation;

public class EmptyAnimationPack implements AnimationPack {

	@Override
	public Animation getEnterAnimation() {
		return null;
	}

	@Override
	public Animation getReturnAnimation() {
		return null;
	}

	@Override
	public Animation getExitAnimation() {
		return null;
	}

	@Override
	public Animation getReenterAnimation() {
		return null;
	}
}
