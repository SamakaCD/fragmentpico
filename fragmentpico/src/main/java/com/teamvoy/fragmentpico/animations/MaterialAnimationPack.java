package com.teamvoy.fragmentpico.animations;

import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

public class MaterialAnimationPack implements AnimationPack {

	private static final float DEFAULT_TRANSLATION_OFFSET = 0.17f;

	@Override
	public Animation getEnterAnimation() {
		AnimationSet animationSet = new AnimationSet(true);
		animationSet.setDuration(300);
		animationSet.setInterpolator(new FastOutSlowInInterpolator());
		animationSet.addAnimation(new AlphaAnimation(0, 1));
		animationSet.addAnimation(new TranslateAnimation(Animation.ABSOLUTE, 0,
				Animation.ABSOLUTE, 0, Animation.RELATIVE_TO_PARENT,
				DEFAULT_TRANSLATION_OFFSET, Animation.RELATIVE_TO_PARENT, 0));
		return animationSet;
	}

	@Override
	public Animation getReturnAnimation() {
		AnimationSet animationSet = new AnimationSet(true);
		animationSet.setDuration(150);
		animationSet.setInterpolator(new FastOutLinearInInterpolator());
		animationSet.addAnimation(new AlphaAnimation(1, 0));
		animationSet.addAnimation(new TranslateAnimation(Animation.ABSOLUTE, 0,
				Animation.ABSOLUTE, 0, Animation.RELATIVE_TO_PARENT,
				0, Animation.RELATIVE_TO_PARENT, DEFAULT_TRANSLATION_OFFSET));
		return animationSet;
	}

	@Override
	public Animation getExitAnimation() {
		return new AnimationStub(350);
	}

	@Override
	public Animation getReenterAnimation() {
		return new AnimationStub(200);
	}

}
