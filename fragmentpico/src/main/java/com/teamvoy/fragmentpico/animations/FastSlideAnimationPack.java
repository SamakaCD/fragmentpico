package com.teamvoy.fragmentpico.animations;

import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

public class FastSlideAnimationPack implements AnimationPack {

	@Override
	public Animation getEnterAnimation() {
		TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
				1, Animation.RELATIVE_TO_PARENT, 0, Animation.ABSOLUTE,
				0, Animation.ABSOLUTE, 0);
		animation.setDuration(250);
		animation.setInterpolator(new FastOutSlowInInterpolator());
		return animation;
	}

	@Override
	public Animation getReturnAnimation() {
		TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
				0, Animation.RELATIVE_TO_PARENT, 1, Animation.ABSOLUTE,
				0, Animation.ABSOLUTE, 0);
		animation.setDuration(250);
		animation.setInterpolator(new FastOutSlowInInterpolator());
		return animation;
	}

	@Override
	public Animation getExitAnimation() {
		TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
				0, Animation.RELATIVE_TO_PARENT, -1, Animation.ABSOLUTE,
				0, Animation.ABSOLUTE, 0);
		animation.setDuration(250);
		animation.setInterpolator(new FastOutSlowInInterpolator());
		return animation;
	}

	@Override
	public Animation getReenterAnimation() {
		TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_PARENT,
				-1, Animation.RELATIVE_TO_PARENT, 0, Animation.ABSOLUTE,
				0, Animation.ABSOLUTE, 0);
		animation.setDuration(250);
		animation.setInterpolator(new FastOutSlowInInterpolator());
		return animation;
	}

}
