package com.teamvoy.fragmentpico.animations;

import android.view.animation.Animation;

public interface AnimationPack {

	Animation getEnterAnimation();

	Animation getReturnAnimation();

	Animation getExitAnimation();

	Animation getReenterAnimation();

}
