package com.teamvoy.fragmentpico.animations;

import android.view.animation.Animation;

public class AnimationStub extends Animation {

	public AnimationStub() {
		super();
	}

	public AnimationStub(int duration) {
		super();
		setDuration(duration);
	}
}
