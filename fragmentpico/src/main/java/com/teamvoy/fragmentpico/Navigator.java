package com.teamvoy.fragmentpico;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Stack;

public class Navigator {

	private Stack<FragmentTransaction> stack;
	private FragmentAttacher fragmentAttacher;

	public Navigator(FragmentAttacher fragmentAttacher) {
		this.stack = new Stack<>();
		this.fragmentAttacher = fragmentAttacher;
	}

	public void add(FragmentTransaction transaction) {
		FragmentTransaction exitFragmentTransaction = stack.isEmpty() ? null : stack.peek();
		if (exitFragmentTransaction != null) {
			exitFragmentTransaction.fragment.onExit();
		}
		stack.add(transaction);
		fragmentAttacher.attach(this, transaction, exitFragmentTransaction);
	}

	public void replace(FragmentTransaction transaction) {
		FragmentTransaction exitFragmentTransaction = stack.isEmpty() ? null : stack.peek();
		if (exitFragmentTransaction != null) {
			exitFragmentTransaction.fragment.onExit();
		}
		stack.set(stack.size() - 1, transaction);
		fragmentAttacher.attach(this, transaction, exitFragmentTransaction);
	}

	public void resetTo(FragmentTransaction... transactions) {
		if (transactions == null || transactions.length == 0) {
			fragmentAttacher.detachAll(this, stack);
			stack.clear();
			return;
		}

		FragmentTransaction topTransaction = transactions[transactions.length - 1];
		FragmentTransaction exitFragmentTransaction = stack.isEmpty() ? null : stack.peek();
		if (exitFragmentTransaction != null) {
			exitFragmentTransaction.fragment.onExit();
		}

		stack.clear();
		FragmentTransaction prevTransaction = null;
		for (int i = 0; i < transactions.length; i++) {
			FragmentTransaction transaction = transactions[i];
			stack.push(transaction);

			if (prevTransaction != null) {
				prevTransaction.fragment.onExit();
			}

			if (i == transactions.length - 1) {
				fragmentAttacher.attach(this, topTransaction, exitFragmentTransaction);
			} else {
				fragmentAttacher.setup(this, transaction, prevTransaction);
			}

			prevTransaction = transaction;
		}
	}

	public void pop() {
		FragmentTransaction transaction = stack.peek();
		boolean willFragmentDetached = transaction.fragment.onRequestDetach();
		if (willFragmentDetached) {
			stack.pop();
			FragmentTransaction reenterFragmentTransaction = stack.isEmpty() ? null : stack.peek();
			if (reenterFragmentTransaction != null) {
				reenterFragmentTransaction.fragment.onReenter();
			}
			fragmentAttacher.detach(this, transaction, reenterFragmentTransaction);
		}
	}

	@Nullable
	public FragmentTransaction findTransactionByTag(Object tag) {
		for (FragmentTransaction transaction : stack) {
			if (transaction.tag == tag || tag.equals(transaction.tag)) {
				return transaction;
			}
		}
		return null;
	}

	public int getFragmentsCount() {
		return stack.size();
	}

	public static void l(Object... objects) {
	    StringBuilder builder = new StringBuilder();
	    for(Object o : objects)
	        builder.append(o).append(" ");
	    Log.d("MyLog", builder.toString());
	}

}
