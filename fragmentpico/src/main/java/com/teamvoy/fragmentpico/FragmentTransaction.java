package com.teamvoy.fragmentpico;

import android.view.animation.Animation;

import com.teamvoy.fragmentpico.animations.AnimationPack;

import java.util.HashMap;
import java.util.Map;

public class FragmentTransaction {

	private static AnimationPack defaultAnimationPack;

	Fragment fragment;
	Animation enterAnimation;
	Animation returnAnimation;
	Animation exitAnimation;
	Animation reenterAnimation;
	Map<String, Object> arguments;
	Object tag;

	public FragmentTransaction(Fragment fragment) {
		this.fragment = fragment;
		this.arguments = new HashMap<>();
		if (defaultAnimationPack != null) {
			withAnimationPack(defaultAnimationPack);
		}
	}

	public static void setDefaultAnimationPack(AnimationPack animationPack) {
		defaultAnimationPack = animationPack;
	}

	public FragmentTransaction withEnterAnimation(Animation enterAnimation) {
		this.enterAnimation = enterAnimation;
		return this;
	}

	public FragmentTransaction withReturnAnimation(Animation returnAnimation) {
		this.returnAnimation = returnAnimation;
		return this;
	}

	public FragmentTransaction withExitAnimation(Animation exitAnimation) {
		this.exitAnimation = exitAnimation;
		return this;
	}

	public FragmentTransaction withReenterAnimation(Animation reenterAnimation) {
		this.reenterAnimation = reenterAnimation;
		return this;
	}

	public FragmentTransaction withAnimationPack(AnimationPack animationPack) {
		return withEnterAnimation(animationPack.getEnterAnimation())
				.withReturnAnimation(animationPack.getReturnAnimation())
				.withExitAnimation(animationPack.getExitAnimation())
				.withReenterAnimation(animationPack.getReenterAnimation());
	}

	public FragmentTransaction withArgument(String key, Object argument) {
		arguments.put(key, argument);
		return this;
	}

	public FragmentTransaction withTag(Object tag) {
		this.tag = tag;
		return this;
	}

	public Map<String, Object> getArguments() {
		return arguments;
	}

}
