package com.teamvoy.fragmentpico;

import java.util.ArrayList;
import java.util.List;

public class Observable {

	private List<Observer> observers;

	public Observable() {
		observers = new ArrayList<>();
	}

	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	public void notifyObservers(Object newValue) {
		for (Observer observer : observers) {
			observer.onValueChanged(newValue);
		}
	}

	public void notifyObservers() {
		notifyObservers(null);
	}

}
