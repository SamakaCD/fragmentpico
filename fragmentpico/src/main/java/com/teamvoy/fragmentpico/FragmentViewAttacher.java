package com.teamvoy.fragmentpico;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;

public class FragmentViewAttacher extends FragmentAttacher {

	private ViewGroup container;
	private int attachedFragmentsCount = 0;
	private boolean detachFirstFragment;
	private boolean animateFirstFragmentEnter;

	/**
	 * A view that was not detached after last Navigator pop and should be detached
	 * during next fragment attach.
	 */
	private View storedLastFragmentView;

	public FragmentViewAttacher(ViewGroup container) {
		this.container = container;
	}

	/**
	 * Enables or disables a behavior, when last fragment of back stack will detach after stack popping.
	 * Disabling this behavior can be used for preventing a blink, during activity finish after
	 * popping all of back stack fragments. This behavior is disabled by default.
	 *
	 * @param detachFirstFragment should this behavior be enabled
	 * @return this {@link FragmentViewAttacher} instance
	 */
	public FragmentViewAttacher setDetachFirstFragment(boolean detachFirstFragment) {
		this.detachFirstFragment = detachFirstFragment;
		return this;
	}

	/**
	 * Enables or disables a behavior when first fragment in back stack should be animated with
	 * enter animation. Disabling this behavior can be used for preventing an appear animation
	 * of first fragment when application starts.
	 *
	 * @param animateFirstFragmentEnter should this behavior be enabled
	 * @return this {@link FragmentViewAttacher} instance
	 */
	public FragmentViewAttacher setAnimateFirstFragmentEnter(boolean animateFirstFragmentEnter) {
		this.animateFirstFragmentEnter = animateFirstFragmentEnter;
		return this;
	}

	@Override
	public void setup(Navigator navigator, FragmentTransaction enterTransaction, @Nullable FragmentTransaction exitTransaction) {
		Fragment fragment = enterTransaction.fragment;
		fragment.setup(container.getContext(), navigator, container, enterTransaction, exitTransaction);
		container.addView(fragment.getView());
		fragment.getView().setVisibility(View.GONE);
		attachedFragmentsCount++;
	}

	@Override
	public void attach(Navigator navigator, final FragmentTransaction enterTransaction, @Nullable final FragmentTransaction exitTransaction) {
		detachStoredLastFragmentView();
		Fragment fragment = enterTransaction.fragment;
		fragment.setup(container.getContext(), navigator, container, enterTransaction, exitTransaction);
		container.addView(fragment.getView());
		attachedFragmentsCount++;

		// Check should be fragment animated, when it is first in back stack.
		// Also always animate, when it is not first in back stack.
		if (animateFirstFragmentEnter | attachedFragmentsCount != 1) {
			fragment.animateEnter(enterTransaction.enterAnimation, new Observable());
		}

		processExitTransaction(exitTransaction);
	}

	@Override
	public void detach(Navigator navigator, final FragmentTransaction returnTransaction, @Nullable FragmentTransaction reenterTransaction) {
		// Check should be first fragment of back stack detached from container
		if (navigator.getFragmentsCount() == 0 && !detachFirstFragment) {
			storedLastFragmentView = returnTransaction.fragment.getView();
			return;
		}

		final Fragment fragment = returnTransaction.fragment;
		Observable animationFinishObservable = new Observable();
		animationFinishObservable.addObserver(new Observer() {
			@Override
			public void onValueChanged(Object o) {
				fragment.onDestroy();
				container.removeView(fragment.getView());
				attachedFragmentsCount--;
			}
		});

		fragment.animateReturn(returnTransaction.returnAnimation, animationFinishObservable);
		processReenterTransaction(reenterTransaction);
	}

	@Override
	public void detachAll(Navigator navigator, Collection<FragmentTransaction> transactions) {
		container.removeAllViews();
	}

	@Override
	public int getAttachedFragmentsCount() {
		return attachedFragmentsCount;
	}

	/**
	 * Detaches a stored last fragment view which was not detached by detachFirstFragment behavior.
	 */
	private void detachStoredLastFragmentView() {
		if (storedLastFragmentView != null) {
			container.removeView(storedLastFragmentView);
			storedLastFragmentView = null;
			attachedFragmentsCount--;
		}
	}

	private void processExitTransaction(final FragmentTransaction transaction) {
		if (transaction != null) {
			Observable exitAnimationFinishObservable = new Observable();
			exitAnimationFinishObservable.addObserver(new Observer() {
				@Override
				public void onValueChanged(Object o) {
					transaction.fragment.getView().setVisibility(View.GONE);
				}
			});
			transaction.fragment.animateExit(transaction.exitAnimation, exitAnimationFinishObservable);
		}
	}

	private void processReenterTransaction(FragmentTransaction transaction) {
		if (transaction != null) {
			transaction.fragment.getView().setVisibility(View.VISIBLE);
			transaction.fragment.animateReenter(transaction.reenterAnimation, new Observable());
		}
	}

}
