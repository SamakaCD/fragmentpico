package com.teamvoy.fragmentpico;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import java.util.Map;

public abstract class Fragment {

	private Context context;
	private Navigator navigator;
	private FragmentTransaction transaction;
	private FragmentTransaction previousTransaction;
	private View view;

	public final FragmentTransaction transact() {
		return new FragmentTransaction(this);
	}

	public void setup(Context context, Navigator navigator, ViewGroup parent, FragmentTransaction transaction, FragmentTransaction previousTransaction) {
		this.context = context;
		this.navigator = navigator;
		this.view = onCreateView(LayoutInflater.from(context), parent);
		this.transaction = transaction;
		this.previousTransaction = previousTransaction;
		onCreate();
	}

	public abstract View onCreateView(LayoutInflater inflater, ViewGroup parent);

	public void onCreate() {

	}

	public void onReenter() {

	}

	public void onExit() {

	}

	public void onDestroy() {

	}

	public boolean onRequestDetach() {
		return true;
	}

	public View getView() {
		return view;
	}

	public Context getContext() {
		return context;
	}

	public Navigator getNavigator() {
		return navigator;
	}

	public FragmentTransaction getTransaction() {
		return transaction;
	}

	public FragmentTransaction getPreviousTransaction() {
		return previousTransaction;
	}

	public Map<String, Object> getArguments() {
		return transaction.getArguments();
	}

	public <T> T getArgument(String key) {
		return (T) getArguments().get(key);
	}

	public int getIntArgument(String key) {
		return getArgument(key);
	}

	public String getStringArgument(String key) {
		return getArgument(key);
	}

	public void animateEnter(@Nullable Animation transactionAnimation, final Observable animationFinishObservable) {
		if (transactionAnimation != null) {
			transactionAnimation.setAnimationListener(new AnimationListenerAdapter() {
				@Override
				public void onAnimationEnd(Animation animation) {
					animationFinishObservable.notifyObservers();
				}
			});
			getView().startAnimation(transactionAnimation);
		} else {
			animationFinishObservable.notifyObservers();
		}
	}

	public void animateReturn(@Nullable Animation transactionAnimation, final Observable animationFinishObservable) {
		if (transactionAnimation != null) {
			transactionAnimation.setAnimationListener(new AnimationListenerAdapter() {
				@Override
				public void onAnimationEnd(Animation animation) {
					animationFinishObservable.notifyObservers();
				}
			});
			getView().startAnimation(transactionAnimation);
		} else {
			animationFinishObservable.notifyObservers();
		}
	}

	public void animateExit(@Nullable Animation transactionAnimation, final Observable animationFinishObservable) {
		if (transactionAnimation != null) {
			transactionAnimation.setAnimationListener(new AnimationListenerAdapter() {
				@Override
				public void onAnimationEnd(Animation animation) {
					animationFinishObservable.notifyObservers();
				}
			});
			getView().startAnimation(transactionAnimation);
		} else {
			animationFinishObservable.notifyObservers();
		}
	}


	public void animateReenter(@Nullable Animation transactionAnimation, final Observable animationFinishObservable) {
		if (transactionAnimation != null) {
			transactionAnimation.setAnimationListener(new AnimationListenerAdapter() {
				@Override
				public void onAnimationEnd(Animation animation) {
					animationFinishObservable.notifyObservers();
				}
			});
			getView().startAnimation(transactionAnimation);
		} else {
			animationFinishObservable.notifyObservers();
		}
	}
}
