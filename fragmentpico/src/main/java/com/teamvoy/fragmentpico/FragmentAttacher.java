package com.teamvoy.fragmentpico;

import android.support.annotation.Nullable;

import java.util.Collection;

public abstract class FragmentAttacher {

	/**
	 * Called when {@link Navigator} required a setup of fragment. {@link Fragment} view should be added
	 * to container, but visibility of this fragment is optional.
	 *
	 * @param navigator        {@link Navigator} which requires {@link Fragment} setup
	 * @param enterTransaction transaction with fragment which should be added
	 * @param exitTransaction  transaction with fragment which placed "under" enterTransaction in back stack.
	 */
	public abstract void setup(Navigator navigator, FragmentTransaction enterTransaction, @Nullable FragmentTransaction exitTransaction);

	/**
	 * Called when {@link Navigator} required an attach of fragment. {@link Fragment} from enterTransaction
	 * should be visible an stay on top of container.
	 *
	 * @param navigator        {@link Navigator} which requires {@link Fragment} setup
	 * @param enterTransaction transaction with fragment which should be attached
	 * @param exitTransaction  with fragment which placed "under" enterTransaction in back stack.
	 */
	public abstract void attach(Navigator navigator, FragmentTransaction enterTransaction, @Nullable FragmentTransaction exitTransaction);

	/**
	 * Called when {@link Navigator} required a detach of fragment. {@link Fragment} from returnTransaction
	 * should be removed from container and fragment from reenterTransaction should be visible and
	 * stay on top of container.
	 *
	 * @param navigator          {@link Navigator} which requires {@link Fragment} setup
	 * @param returnTransaction  transaction with fragment which should be removed from container
	 * @param reenterTransaction transaction with fragment which was "under" removing fragment and
	 *                           now should be stay on top of container. May be <b>null</b> if
	 *                           removing fragment was last in back stack.
	 */
	public abstract void detach(Navigator navigator, FragmentTransaction returnTransaction, @Nullable FragmentTransaction reenterTransaction);

	/**
	 * Called when {@link Navigator} required a detach of all attached fragments.
	 *
	 * @param navigator    {@link Navigator} which requires {@link Fragment} setup
	 * @param transactions {@link Collection} with transactions which should be detached from container
	 */
	public abstract void detachAll(Navigator navigator, Collection<FragmentTransaction> transactions);

	/**
	 * Returns count of attached fragments.
	 *
	 * @return count of attached fragments
	 */
	public abstract int getAttachedFragmentsCount();

}
