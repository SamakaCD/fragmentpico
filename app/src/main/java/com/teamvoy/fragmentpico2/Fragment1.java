package com.teamvoy.fragmentpico2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teamvoy.fragmentpico.Fragment;

public class Fragment1 extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent) {
		return inflater.inflate(R.layout.fragment_1, parent, false);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		final int i = getIntArgument("arg");
		((TextView) getView().findViewById(R.id.text)).setText("" + i);

		getView().findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

			}
		});

		getView().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (i == 2) {
					getNavigator().resetTo(new Fragment1().transact().withArgument("arg", i + 1));
				} else {
					getNavigator().add(new Fragment1().transact().withArgument("arg", i + 1));
				}
			}
		});
	}

	@Override
	public String toString() {
		return "Fragment" + getIntArgument("arg");
	}
}
