package com.teamvoy.fragmentpico2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.teamvoy.fragmentpico.FragmentTransaction;
import com.teamvoy.fragmentpico.FragmentViewAttacher;
import com.teamvoy.fragmentpico.Navigator;
import com.teamvoy.fragmentpico.animations.MaterialAnimationPack;

public class MainActivity extends AppCompatActivity {

	Navigator navigator;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FragmentTransaction.setDefaultAnimationPack(new MaterialAnimationPack());
		FragmentViewAttacher attacher = new FragmentViewAttacher((ViewGroup) findViewById(R.id.content));
		navigator = new Navigator(attacher);
		navigator.resetTo(
				new Fragment1().transact().withArgument("arg", 1),
				new Fragment1().transact().withArgument("arg", 2),
				new Fragment1().transact().withArgument("arg", 3)
		);
	}

	@Override
	public void onBackPressed() {
		navigator.pop();
		if (navigator.getFragmentsCount() == 0) {
			super.onBackPressed();
		}
	}
}
